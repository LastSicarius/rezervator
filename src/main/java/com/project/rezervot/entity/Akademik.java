package com.project.rezervot.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "akademik")
public class Akademik {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    private Integer numberOfRooms;

    @OneToMany(cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Room> rooms;

    public Akademik()
    {

    }

    public Akademik(String name, Integer numberOfRooms, List<Room> rooms) {
        this.name = name;
        this.numberOfRooms = numberOfRooms;
        this.rooms = rooms;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getNumberOfRooms() {
        return numberOfRooms;
    }

    public void setNumberOfRooms(Integer numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }
}
