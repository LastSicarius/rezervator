package com.project.rezervot.entity;

import javax.persistence.*;

@Entity
@Table(name = "reservation")
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @OneToOne
    private Payment payment;

    @OneToOne
    private Room room;

    @OneToOne
    private User user;

    @OneToOne
    private Akademik akademik;

    private String status;

    public Reservation(){
        this.status = "nie opłacony";
    }

    public Reservation(String name, Room room, Akademik akademik, final User user, final Payment payment) {
        this.name = name;
        this.room = room;
        this.akademik = akademik;
        this.user = user;
        this.payment = payment;
        this.status = "nie zaakceptowana";
    }

    public Akademik getAkademik()
    {
        return akademik;
    }

    public Integer getId() {
        return id;
    }

    public String getStatus()
    {
        return status;
    }

    public User getUser()
    {
        return user;
    }

    public void setAkademik(final Akademik akademik)
    {
        this.akademik = akademik;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public void setStatus(final String status)
    {
        this.status = status;
    }

    public void setUser(final User user)
    {
        this.user = user;
    }
}
