package com.project.rezervot.entity;

import javax.persistence.*;

@Entity
@Table(name = "payment")
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer value;

    private String status;

    private String znizka;

    public Payment()
    {

    }

    public Payment(Integer value, String status, String znizka) {
        this.value = value;
        this.status = status;
        this.znizka = znizka;
    }

    public Integer getId() {
        return id;
    }

    public String getZnizka()
    {
        return znizka;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        status = status;
    }

    public void setZnizka(final String znizka)
    {
        this.znizka = znizka;
    }
}
