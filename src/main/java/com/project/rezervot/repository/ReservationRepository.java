package com.project.rezervot.repository;

import org.springframework.data.repository.CrudRepository;

import com.project.rezervot.entity.Reservation;

public interface ReservationRepository extends CrudRepository<Reservation, Integer>
{

}
