package com.project.rezervot.repository;

import org.springframework.data.repository.CrudRepository;

import com.project.rezervot.entity.Payment;

public interface PaymentRepository extends CrudRepository<Payment, Integer>
{
}
