package com.project.rezervot.repository;

import org.springframework.data.repository.CrudRepository;

import com.project.rezervot.entity.Room;

public interface RoomRepository extends CrudRepository<Room, Integer>
{
}
