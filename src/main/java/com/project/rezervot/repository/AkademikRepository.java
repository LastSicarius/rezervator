package com.project.rezervot.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.project.rezervot.entity.Akademik;

@Repository
public interface AkademikRepository extends CrudRepository<Akademik, Integer>
{

}
