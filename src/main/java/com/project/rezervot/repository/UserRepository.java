package com.project.rezervot.repository;

import com.project.rezervot.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {

    User findUserByUserName(String userName);

    Integer deleteUserByUserName(String name);
}
