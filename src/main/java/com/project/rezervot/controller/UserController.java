package com.project.rezervot.controller;

import com.project.rezervot.entity.User;
import com.project.rezervot.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;

@RequestMapping(value = "user")
@RestController
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "/login")
    public String login(@RequestParam final String name, @RequestParam final String password)
    {
        final User userFromDb = userService.findUserByUserName(name);
        if (userFromDb == null)
        {
            return "no user with this name";
        }
        if (!userFromDb.getPassword().equals(password))
        {
            return "password not match";
        }
        return "user found";
    }
    @PostMapping(value = "/register")
    public String register(@RequestParam final String name, @RequestParam final String password)
    {
        final boolean userNameUsed = userService.findUserByUserName(name) != null;
        if (userNameUsed)
        {
            return "userName used";
        }
        userService.createUser(name, password);
        return "user registered";
    }

    @PostMapping(value = "/logout")
    public void logout()
    {

    }

    @DeleteMapping(value = "/delete")
    public String delete(@RequestParam final String name)
    {
        final boolean userDeleted = userService.deleteUser(name);
        if(userDeleted)
        {
            return "user removed";
        }
        return "no user with this name";
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public String handleMissingParam(final MissingServletRequestParameterException ex)
    {
        return "Error";
    }
}
