package com.project.rezervot.controller.wrapper;

public class ReservationWrapper
{
    private Integer akademik;

    private Integer room;


    public ReservationWrapper()
    {

    }

    public Integer getAkademik()
    {
        return akademik;
    }

    public Integer getRoom()
    {
        return room;
    }

    public void setAkademik(final Integer akademik)
    {
        this.akademik = akademik;
    }

    public void setRoom(final Integer room)
    {
        this.room = room;
    }
}
