package com.project.rezervot.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.project.rezervot.controller.wrapper.RegisterWrapper;
import com.project.rezervot.controller.wrapper.ReservationWrapper;
import com.project.rezervot.entity.Akademik;
import com.project.rezervot.entity.Payment;
import com.project.rezervot.entity.Reservation;
import com.project.rezervot.entity.Room;
import com.project.rezervot.service.AkademikService;
import com.project.rezervot.service.PaymentService;
import com.project.rezervot.service.RoomService;
import com.project.rezervot.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.project.rezervot.service.ReservationService;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {

    private final InMemoryUserDetailsManager inMemoryUserDetailsManager;

    final ReservationService reservationService;

    final UserService userService;

    final AkademikService akademikService;

    final RoomService roomService;

    final PaymentService paymentService;

    @Autowired
    public HomeController(InMemoryUserDetailsManager inMemoryUserDetailsManager, final ReservationService reservationService,
                          UserService userService, final AkademikService akademikService,
                          final RoomService roomService, final PaymentService paymentService)
    {
        this.inMemoryUserDetailsManager = inMemoryUserDetailsManager;
        this.reservationService = reservationService;
        this.userService = userService;
        this.akademikService = akademikService;
        this.roomService = roomService;
        this.paymentService = paymentService;
    }

    @GetMapping("/")
    public String root() {
        return "index";
    }

    @GetMapping("/user")
    public String userIndex() {
        return "user/index";
    }

    @GetMapping("/manager")
    public String managerIndex()
    {
        return "manager/index";
    }

    @GetMapping("/reservation_management")
    public String reservationManagement(Model model) {
        model.addAttribute("reservations", reservationService.findAll());
        return "manager/reservation_management";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/access-denied")
    public String accessDenied() {
        return "/error/access-denied";
    }

    @GetMapping("/registration")
    public String registrationForm(Model model)
    {
        model.addAttribute("registerWrapper", new RegisterWrapper());
        return "registration";
    }

    @PostMapping("/registration")
    public String userRegistration(@ModelAttribute RegisterWrapper registerWrapper, Model model)
    {
        final String password1 = registerWrapper.getPassword();
        final String password2 = registerWrapper.getPassword2();
        if (password1.isEmpty() || password2.isEmpty() || !password1.equals(password2)) {
            model.addAttribute("passwordNotMatch", "true");
            return "registration";
        }
        if(userService.findUserByUserName(registerWrapper.getUserName()) != null)
        {
            model.addAttribute("userAlreadyInDb", "true");
            return "registration";
        }
        inMemoryUserDetailsManager.createUser(User.withUsername(registerWrapper.getUserName())
                                                  .password("{noop}" + password1)
                                                  .roles("USER")
                                                  .build());
        userService.createUser(registerWrapper.getUserName(), password1);
        return "registration";
    }

    @GetMapping("/reservation-form")
    public String reservationForm(Model model)
    {
        model.addAttribute("akademiki", akademikService.findAll());
        model.addAttribute("reservationWrapper", new ReservationWrapper());
        return "user/reservation-form";
    }

    @PostMapping("/reservation")
    public String reservation(@ModelAttribute ReservationWrapper reservationWrapper, Model model)
    {
        final String loggedUser = SecurityContextHolder.getContext().getAuthentication().getName();
        final com.project.rezervot.entity.User user = userService.findUserByUserName(loggedUser);
        final Akademik akademik = akademikService.findById(reservationWrapper.getAkademik()).get();
        final Room room = roomService.findbyId(reservationWrapper.getRoom()).get();
        final Payment payment = new Payment(100, "nieoplacony", "brak");
        final Reservation reservation = new Reservation("newRes", room, akademik, user, payment);
        paymentService.save(payment);
        reservationService.save(reservation);
        model.addAttribute("created","true");
        return "user/reservation-form";
    }

    @GetMapping("/rooms")
    @ResponseBody
    public List<Room> getRoomsForAkademik(@RequestParam Integer akademikId)
    {
        final Optional<Akademik> akademik = akademikService.findById(akademikId);
        if(akademik.isPresent())
        {
            return akademik.get().getRooms();
        }
        return new ArrayList<>();
    }

}
