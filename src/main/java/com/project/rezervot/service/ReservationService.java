package com.project.rezervot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.project.rezervot.entity.Reservation;
import com.project.rezervot.repository.ReservationRepository;

@Service
@Transactional
public class ReservationService
{
    private final ReservationRepository reservationRepository;

    @Autowired
    public ReservationService(final ReservationRepository reservationRepository)
    {
        this.reservationRepository = reservationRepository;
    }

    public List<Reservation> findAll()
    {
        return (List<Reservation>) reservationRepository.findAll();
    }

    public void save(final Reservation reservation)
    {
        reservationRepository.save(reservation);
    }
}
