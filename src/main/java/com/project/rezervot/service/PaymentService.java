package com.project.rezervot.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.project.rezervot.entity.Payment;
import com.project.rezervot.repository.PaymentRepository;

@Service
@Transactional
public class PaymentService
{
    private final PaymentRepository paymentRepository;

    @Autowired
    public PaymentService(final PaymentRepository paymentRepository)
    {
        this.paymentRepository = paymentRepository;
    }

    public void save(final Payment payment)
    {
        paymentRepository.save(payment);
    }
}
