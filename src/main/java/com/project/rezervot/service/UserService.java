package com.project.rezervot.service;


import com.project.rezervot.entity.User;
import com.project.rezervot.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User findUserByUserName(final String userName)
    {
    return userRepository.findUserByUserName(userName);
    }

    public void createUser(String name, String password) {
        final User user = new User(null, name, password);
        userRepository.save(user);
    }

    public List<User> findAll()
    {
        return (List<User>) userRepository.findAll();
    }

    public boolean deleteUser(String name) {
        return userRepository.deleteUserByUserName(name) == 1;
    }
}
