package com.project.rezervot.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.project.rezervot.entity.Akademik;
import com.project.rezervot.repository.AkademikRepository;

@Service
@Transactional
public class AkademikService
{
    private final AkademikRepository akademikRepository;

    @Autowired
    public AkademikService(final AkademikRepository akademikRepository)
    {
        this.akademikRepository = akademikRepository;
    }

    public List<Akademik> findAll()
    {
        return (List<Akademik>) akademikRepository.findAll();
    }

    public Optional<Akademik> findById(final Integer id)
    {
        return akademikRepository.findById(id);
    }
}
