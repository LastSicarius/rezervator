package com.project.rezervot.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.project.rezervot.entity.Room;
import com.project.rezervot.repository.RoomRepository;

@Service
@Transactional
public class RoomService
{
    private final RoomRepository roomRepository;

    @Autowired
    public RoomService(final RoomRepository roomRepository)
    {
        this.roomRepository = roomRepository;
    }

    public Optional<Room> findbyId(final Integer id)
    {
        return roomRepository.findById(id);
    }
}
