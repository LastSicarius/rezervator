package com.project.rezervot.config;

import com.project.rezervot.entity.User;
import com.project.rezervot.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.InMemoryUserDetailsManagerConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter
{
    @Autowired
    private LoggingAccessDeniedHandler accessDeniedHandler;

    @Autowired
    private SuccessHandler successHandler;

    @Autowired
    private UserService userService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
            .antMatchers("/h2-console/**").hasRole("MANAGER")//allow h2 console access to admins only
            .antMatchers("/h2-console/login.do").hasRole("MANAGER")//allow h2 console access to admins only
            .antMatchers(
                "/",
                "/registration",
                "/js/**",
                "/css/**",
                "/img/**",
                "/webjars/**").permitAll()
            .antMatchers("/user/**", "/reservation-form").hasRole("USER")
            .antMatchers("/manager/**").hasRole("MANAGER")
            .anyRequest().authenticated()
            .and()
            .formLogin()
            .loginPage("/login")
            .loginProcessingUrl("/login")
            .successHandler(successHandler)
            .permitAll()
            .and()
            .logout()
            .invalidateHttpSession(true)
            .clearAuthentication(true)
            .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
            .logoutSuccessUrl("/login?logout")
            .permitAll()
            .and()
            .headers()
            .frameOptions()
            .disable()
            .and()
        .csrf().ignoringAntMatchers("/h2-console/**").and()
            .exceptionHandling()
            .accessDeniedHandler(accessDeniedHandler);


    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(inMemoryUserDetailsManager());
    }

    @Bean
    public InMemoryUserDetailsManager inMemoryUserDetailsManager() {
        List<UserDetails> userDetailsList = new ArrayList<>();
        final List<User> userList = userService.findAll();

        for(final User user: userList)
        {
            userDetailsList.add(org.springframework.security.core.userdetails.User.withUsername(user.getUserName()).password("{noop}"+user.getPassword()).roles(user.getRole()).build());
            //configurer.withUser(user.getUserName()).password("{noop}" + user.getPassword()).roles(user.getRole());
        }
        return new InMemoryUserDetailsManager(userDetailsList);
    }

}
