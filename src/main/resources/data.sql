--insert akademiks
insert into akademik( name, number_of_rooms) values
('Akademik US', 20),
('Akademik ZUT', 30),
('Akademik PUM', 40);
--insert rooms
insert into room(number, status) values
(2, 'wolny'),
(3, 'zajety'),
(4, 'wolny');
--insert payment
insert into payment( value, status, znizka) values
(3, 'oplacony', 'student'),
(4, 'nie oplacony', 'brak'),
(5, 'oplacony', 'student');
-- --insert userDetails
insert into user_details(name, second_name, phone_number, address, pesel)  values
('test', 'test', 1234, 'test', 123456789),
( 'test', 'test', 1234, 'test', 123456789),
( 'test', 'test', 1234, 'test', 123456789),
( 'manager', 'manager', 1234, 'test', 123456789);
--insert users
insert into user(user_details_id, user_name, password, role) values
(1,'test','test', 'USER'),
(2,'test1','test', 'USER'),
(3,'test2','test', 'USER'),
(4, 'manager', 'password', 'MANAGER');
--insert reservations
INSERT INTO reservation(name, payment_id, room_id, user_id, status, akademik_id) values
('123',1,1,1, 'nie zaakceptowana',1),
('321',2,2,2, 'Odrzucona',2),
('231',2,2,3, 'Zaakceptowana',3);
--relate akademik with rooms
insert into akademik_rooms(akademik_id, rooms_id) values
(1, 1),
(2, 2),
(3, 3);